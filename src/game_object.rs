use super::card_drawable::CardDrawable;
use super::Images;
use ggez::Context;
use ggez::graphics::Rect;

pub trait GameObject {
    fn draw(&self, ctx: &mut Context, images: &Images);

    fn update(&mut self, ctx: &Context);

    fn set_pos(&mut self, x: f32, y: f32);
    fn get_rect(&self) -> &Rect;

    fn card(&mut self) -> Option<&mut CardDrawable>;

    fn in_bounds(&self, x: f32, y: f32) -> bool {
        let rect = self.get_rect();
        x > rect.x && x < rect.x + rect.w &&
        y > rect.y && y < rect.y + rect.h
    }
}
