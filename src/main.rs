extern crate rand;
#[macro_use]
#[allow(deprecated)]
extern crate rand_derive;
#[macro_use]
extern crate derive_new;
extern crate ggez;

mod card;
mod card_drawable;
mod game_object;

use ggez::{Context, ContextBuilder, GameResult};
use ggez::event::{self, MouseButton};
use ggez::graphics::{self, Image};
use ggez::timer::get_time_since_start;
use floating_duration::TimeAsFloat;
use self::card_drawable::CardDrawable;
use self::game_object::GameObject;

struct MainState {
    game_objects: Vec<Box<GameObject>>,
    images: Images,
}

pub struct Images {
    oval_solid        : Image,
    oval_striped      : Image,
    oval_outlined     : Image,
    squiggle_solid    : Image,
    squiggle_striped  : Image,
    squiggle_outlined : Image,
    diamond_solid     : Image,
    diamond_striped   : Image,
    diamond_outlined  : Image,
}

impl MainState {
    fn new(ctx: &mut Context) -> GameResult<MainState> {
        let images = Images {
            oval_solid    : Image::new(ctx, "/oval_solid.png").unwrap(),
            oval_striped  : Image::new(ctx, "/oval_striped.png").unwrap(),
            oval_outlined : Image::new(ctx, "/oval_outlined.png").unwrap(),
            squiggle_solid    : Image::new(ctx, "/squiggle_solid.png").unwrap(),
            squiggle_striped  : Image::new(ctx, "/squiggle_striped.png").unwrap(),
            squiggle_outlined : Image::new(ctx, "/squiggle_outlined.png").unwrap(),
            diamond_solid    : Image::new(ctx, "/diamond_solid.png").unwrap(),
            diamond_striped  : Image::new(ctx, "/diamond_striped.png").unwrap(),
            diamond_outlined : Image::new(ctx, "/diamond_outlined.png").unwrap(),
        };

        let s = MainState {
            game_objects: Vec::new(),
            images,
        };
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.game_objects.len() < 12 {
            let time = get_time_since_start(ctx).as_fractional_secs();
            if time > self.game_objects.len() as f64 / 4.0 {
                let mut card = CardDrawable::new(0.0, 0.0);
                let count = self.game_objects.len();
                card.set_pos((count%3) as f32*200.0 + 50.0, ((count-count%3)/3) as f32*150.0 + 50.0);

                self.game_objects.push(Box::new(card));
            }
        }

       for game_object in &mut self.game_objects {
           game_object.update(ctx);
       }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);

        for game_object in &self.game_objects {
            game_object.draw(ctx, &self.images);
        }

        graphics::present(ctx);
        Ok(())
    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        _button: MouseButton,
        x: i32,
        y: i32
    ) {
        for game_object in &mut self.game_objects {
            if game_object.in_bounds(x as f32, y as f32) {
                if let Some(card) = game_object.card() {
                    //Eventually check if three cards are selected and check if they're a set
                }
            }
        }
    }
}

pub fn main() {
    let ctx = &mut ContextBuilder::new("Set", "Emile")
        .window_setup(ggez::conf::WindowSetup::default().title("Set!"))
        .window_mode(ggez::conf::WindowMode::default().dimensions(650, 650))
        .build().expect("Failed to build ggez context");

    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
