use ggez::{Context, timer::get_delta};
use ggez::graphics::{self, Point2, Rect, DrawMode, Color, DrawParam};
use rand::random;
use floating_duration::TimeAsFloat;
use super::game_object::GameObject;
use super::card::Card;
use super::Images;

pub struct CardDrawable {
    card: Card,
    //This is the position it is supposed to be
    x: f32,
    y: f32,
    //This is where it's rendered
    rect: Rect,
}

const WIDTH: f32 = 150.0;
const HEIGHT: f32 = 100.0;
const SPEED: f32 = 4.0;

impl CardDrawable {
    pub fn new(x: f32, y: f32) -> CardDrawable {
        CardDrawable {
            card: random::<Card>(),
            x,
            y,
            rect: Rect {
                x,
                y,
                w: WIDTH,
                h: HEIGHT,
            },
        }
    }
}


impl GameObject for CardDrawable {
    fn draw(&self, ctx: &mut Context, images: &Images) {
        graphics::set_color(ctx, Color::new(1.0, 1.0, 1.0, 1.0)).unwrap();
        graphics::rectangle(
            ctx,
            DrawMode::Fill,
            self.rect,
        ).unwrap();

        graphics::set_color(ctx, self.card.color.value()).unwrap();

        let amount = self.card.number.value();

        for i in 0..amount {
            let mut params = DrawParam::default();
            params.dest = Point2::new(
                //self.rect.x is the rendered squares' position
                self.rect.x+WIDTH/2.0
                //If there's only 1 shape, this bit does nothing, but if there's two it draws the
                //first shape 15 "pixels" to the left (-15.0 pixels), and the second one 15 to the
                //right (30.0-15.0 pixels), and so on.
                +(30*i -15*(amount-1)) as f32,
                self.rect.y+HEIGHT/2.0,
            );
            params.scale = Point2::new(
                0.25,
                0.25,
            );
            params.offset = Point2::new(
                0.5,
                0.5,
            );

            use super::card::{Symbol::*, Shading::*};
            let image = match self.card.symbol {
                Oval => match self.card.shading {
                    Solid    => &images.oval_solid,
                    Striped  => &images.oval_striped,
                    Outlined => &images.oval_outlined,
                },
                Squiggle => match self.card.shading {
                    Solid    => &images.squiggle_solid,
                    Striped  => &images.squiggle_striped,
                    Outlined => &images.squiggle_outlined,
                },
                Diamond => match self.card.shading {
                    Solid    => &images.diamond_solid,
                    Striped  => &images.diamond_striped,
                    Outlined => &images.diamond_outlined,
                }
            };

            graphics::draw_ex(
                ctx,
                image,
                params,
            ).unwrap();
        }
    }

    fn set_pos(&mut self, x: f32, y: f32) {
        self.x = x;
        self.y = y;
    }

    fn get_rect(&self) -> &Rect {
        &self.rect
    }

    fn update(&mut self, ctx: &Context) {
        let dt = get_delta(ctx).as_fractional_secs() as f32;

        //Lerp towards
        self.rect.x = (1.0 - dt * SPEED) * self.rect.x + dt * SPEED * self.x;
        self.rect.y = (1.0 - dt * SPEED) * self.rect.y + dt * SPEED * self.y;
    }

    fn card(&mut self) -> Option<&mut CardDrawable> {
        Some(self)
    }
}
