use ggez::graphics;

#[derive(Debug, Rand, PartialEq)]
pub enum Color {
    Red,
    Purple,
    Green,
}

impl Color {
    pub fn value(&self) -> graphics::Color {
        use self::Color::*;
        match self {
            Red    => RED,
            Purple => PURPLE,
            Green  => GREEN,
        }
    }
}

const RED: graphics::Color = graphics::Color {
    r: 0.906,
    g: 0.282,
    b: 0.298,
    a: 1.000,
};
const PURPLE: graphics::Color = graphics::Color {
    r: 0.341,
    g: 0.255,
    b: 0.580,
    a: 1.000,
};
const GREEN: graphics::Color = graphics::Color {
    r: 0.227,
    g: 0.686,
    b: 0.337,
    a: 1.000,
};

#[derive(Debug, Rand, PartialEq)]
pub enum Symbol {
    Oval,
    Squiggle,
    Diamond,
}

#[derive(Debug, Rand, PartialEq)]
pub enum Number {
    One,
    Two,
    Three,
}

impl Number {
    pub fn value(&self) -> i64 {
        use self::Number::*;
        match self {
            One =>   1,
            Two =>   2,
            Three => 3,
        }
    }
}

#[derive(Debug, Rand, PartialEq)]
pub enum Shading {
    Solid,
    Striped,
    Outlined,
}

fn is_partial_set<T: PartialEq>(a: &T, b: &T, c: &T) -> bool {
    if a == b {
        return a == c;
    } else {
        return a != c && b != c;
    }
}

pub fn is_set(a: &Card, b: &Card, c: &Card) -> bool {
    is_partial_set(&a.color, &b.color, &c.color)
    && is_partial_set(&a.symbol, &b.symbol, &c.symbol)
    && is_partial_set(&a.number, &b.number, &c.number)
    && is_partial_set(&a.shading, &b.shading, &c.shading)
}

#[derive(Debug, Rand, new)]
pub struct Card {
    pub color: Color,
    pub symbol: Symbol,
    pub number: Number,
    pub shading: Shading,
}
