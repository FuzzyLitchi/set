# Set
Literally the game "Set" but RIIR. If you need the rules [Wikipedia](https://en.wikipedia.org/wiki/Set_(card_game)) has them.

## Running the game
You need rust, I'm using cargo 1.32.0-nightly as of writing but it probably works fine with whatever as long as it's new-ish. Anyway you need to run `ln -s $(pwd)/resources target/release/resources` so that the binary is "next" to the resources folder, but we're just linking it instead. Then run `cargo run --release` you should always run it as release as performance matters since it's a game, even when testing. If that doesn't work \@me or don't
